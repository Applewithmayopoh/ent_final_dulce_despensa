from django.urls import path, include
from . import views

urlpatterns = [   
    # localhost:8000
    path('listar_posts', views.post_list, name='listar_posts'),
    
    # localhost:8000/post/numero_de_pk
    path('<int:pk>/', views.post_detail, name='post_detail'),
    
    # localhost:8000/post/new
    path('new/', views.post_new, name='post_new'),
    
    # localhost:8000/post/numero_de_pk/edit
    path('<int:pk>/edit/', views.post_edit, name='post_edit'),
]